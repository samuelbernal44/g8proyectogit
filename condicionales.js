function saludo() {
    let time = new Date().getHours();
    let saludo;
    let nombre;

    nombre = prompt('Ingrese su nombre: ', '');

    if (time < 12) {
        saludo = "Buen día";
    } else if (time < 18) {
        saludo = "Buena tarde";
    } else {
        saludo = "Buena noche";
    }

    document.write("Teniendo en cuenta que son las " + time + " horas, el sistema le dice, " + saludo + " tripulante " + nombre);
}

function fechaHora() {
    let time = new Date().getHours();
    let dia;
    let fecha = new Date();
    switch (new Date().getDay()) {
        case 0:
            dia = "Domingo";
            break;
        case 1:
            dia = "Lunes";
            break;
        case 2:
            dia = "Martes";
            break;
        case 3:
            dia = "Miercoles";
            break;
        case 4:
            dia = "Jueves";
            break;
        case 5:
            dia = "Viernes";
            break;
        case 6:
            dia = "Sábado";
            break;
    }
    document.write("Hoy es " + dia + " " + fecha.toLocaleDateString() + " y son las " + time);
}

function cicloFor() {
    let text = "";
    for (let i = 0; i < 5; i++) {
        text += "El número es " + i + "<br>";
    }
    document.write(text);
}

function cicloWhile() {
    let text = "";
    let i = 0;
    while (i < 10) {
        text += "<br>El número es " + i;
        i++;
    }
    document.write(text);
}

function cicloDoWhile() {
    let text = "";
    let i = 0;
    do {
        text += "<br>El número es " + i;
        i++;
    }
    while (i < 20);
    document.write(text);
}
